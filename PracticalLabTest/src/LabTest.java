import java.util.Scanner;

public class LabTest {

    private double precision;
    private double pi;

    public void getValue() {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter precision: ");
        this.precision = s.nextDouble();
        while(this.precision < 0 || this.precision > 1) {
            System.out.print("Enter the precision that is between 0 and 1: ");
            this.precision = s.nextDouble();
        }
    }

    public void calculatePi() {
        double count = 0;
        double sign = 1;
        double term = 4;
        this.pi = term;
        while (term > this.precision) {
            count++;
            sign *= -1;
            term = 4 / (2 * count +1);
            this.pi = pi + term * sign;
        }
    }

    public void display() {
        System.out.println("The value of pi to " + this.precision + " precision is " + this.pi);
    }
}
