import java.util.Scanner;
import java.text.DecimalFormat;
/*
* Assignment #1
* This program prompts user to enter the scores of every assignment, then calculate the final score.
final grade
* Name: Ziang Cui
* Section: 18W_CST8110_311
* Lab Teacher: Angela Giddings
* Date: Feb 16,2018
* */
public class Assign1 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        DecimalFormat formatter = new DecimalFormat("123.456E3");
        formatter.setDecimalSeparatorAlwaysShown(false);

        //declaring variables
        double labAssignments, hybridActivities, test, finalAssessment;

        System.out.print  ("Welcome to the CST8101 Final Mark Calculator\n\n");
        System.out.print  ("Enter your Lab Assignments mark out of 25: ");
        labAssignments = input.nextDouble();
        System.out.print  ("Enter your Hybrid Activities / Quizzes mark out of 20: ");
        hybridActivities = input.nextDouble();
        System.out.print  ("Enter your Test mark out of 25: ");
        test = input.nextDouble();
        System.out.printf  ("Enter your Final Assessment (Exam) mark out of 30: ");
        finalAssessment = input.nextDouble();



        System.out.println  ("\nTheory grade: "+formatter.format((test+finalAssessment)/55));
        System.out.println  ("Practical grade: "+formatter.format((labAssignments+hybridActivities)/45));
        System.out.println  ("Final grade: "+formatter.format((test+finalAssessment+labAssignments+hybridActivities)/100));

    }
}