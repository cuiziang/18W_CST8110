/**  This program prompts user to enter a dates, then calculate the difference
 *   Author:   Ziang cui
 *   CST8110 Introduction to Computer Programming
 */

public class Assign2 {
    public static void main(String[] args) {

        DateCalculator d = new DateCalculator();

        d.inputDates();
        d.calculateDifference();
        d.display();

    }
}