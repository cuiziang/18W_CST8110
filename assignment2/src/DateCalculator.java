public class DateCalculator {
    private OurDate firstDate;
    private OurDate secondDate;
    private String message;

    public DateCalculator(){
        this.firstDate = new OurDate();
        this.secondDate = new OurDate();
        this.message = "";
        System.out.println("Date Calculator - What's the difference between me and you?\n");
    }

    public void inputDates(){
        System.out.println("Enter first date");
        this.inputDate(firstDate);
        System.out.println("Enter second date");
        this.inputDate(secondDate);
    }

    private void inputDate(OurDate o){
        o.inputYear();
        o.inputMonth();
        o.inputDay();
        System.out.println("");
    }

    public void calculateDifference(){
        int diff = this.firstDate.calcDays() - this.secondDate.calcDays();
        if(diff == 0){
            this.message = " is the same as ";
        }
        else if (diff > 0){
            if (diff >= 720) {
                this.message = " is " + diff / 360 + " years later than ";
            }
            else if (diff >= 360){
                this.message = " is 1 year later than ";
            }
            else if (diff >= 60){
                this.message = " is " + diff/30 +" months later than ";
            }
            else if (diff >= 30){
                this.message = " is 1 month later than ";
            }
            else if (diff >= 14){
                this.message = " is " + diff/7 +" weeks later than ";
            }
            else if (diff >= 7){
                this.message = " is 1 week later than ";
            }
            else if (diff > 1){
                this.message = " is " + diff +" days later than ";
            }
            else {
                this.message = " is 1 day later than ";
            }
        }
        else {
            diff *= -1;
            if (diff >= 720) {
                this.message = " is " + diff / 360 + " years earlier than ";
            }
            else if (diff >= 360) {
                this.message = " is 1 year earlier than ";
            }
            else if (diff >= 60) {
                this.message = " is " + diff / 30 + " months earlier than ";
            }
            else if (diff >= 30) {
                this.message = " is 1 month earlier than ";
            }
            else if (diff >= 14){
                this.message = " is " + diff/7 +" weeks earlier than ";
            }
            else if (diff >= 7){
                this.message = " is 1 week earlier than ";
            }
            else if (diff > 1) {
                this.message = " is " + diff + " days earlier than ";
            }
            else {
                this.message = " is 1 day earlier than ";
            }
        }
    }

    public void display(){
        this.firstDate.displayDate();
        System.out.print(this.message);
        this.secondDate.displayDate();
    }
}