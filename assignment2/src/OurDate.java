import java.util.*;

public class OurDate{
    private int year;
    private int month;
    private int day;
    private Scanner s;

    public OurDate(){
        this(2000,1,1);
    }

    public OurDate(int y, int m, int d){
        s = new Scanner(System.in);
        this.year = y;
        this.month = m;
        this.day = d;
    }

    public void inputYear() {
        System.out.print("Enter a year: ");
        year = s.nextInt();
    }

    public void inputMonth() {
        System.out.print("Enter a month: ");
        month = s.nextInt();
    }

    public void inputDay( ) {
        System.out.print("Enter a day: ");
        day = s.nextInt();
    }


    public void displayDate() {
        System.out.print (year+"/"+month+"/"+day);
    }

    public int calcDays() {
        return ((year-2000)*360+(month-1)*30+(day-1));
    }
}


