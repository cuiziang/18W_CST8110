/**  This problem will simulate a dice betting game
 *   Author:   Ziang cui
 *   CST8110 Introduction to Computer Programming
 */

public class Assign3 {
    public static void main(String[] args) {
        Game g = new Game();
        g.playGame();
    }
}
