import java.util.Random;

public class Die {
    // holds the value of the ONE die
    private int dieValue;
    // common random number generator
    private Random randomNumbers;
    // default (no-arg) constructor
    public Die() {
        this.dieValue = 0;
        this.randomNumbers = new Random();
    }
    // Generate a random value for die
    public void rollDie() {
        this.dieValue=randomNumbers.nextInt(6)+1;
    }
    // Displays the value of die
    public void displayDie() {
        System.out.print(this.dieValue);
    }
    // Returns value of die
    public int getValue()     {
        return this.dieValue;
    }
}
