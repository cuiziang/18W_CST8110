import java.util.Scanner;

public class Game {
    // holds the value of the pot
    private int potAmount;
    // holds the bet value
    private int betAmount;
    // holds the bet type
    private String betType;
    // common scanner
    private Scanner input;

    // default (no-arg) constructor
    public Game() {
        System.out.println("Welcome to Double or Nothing Dice Game..bet an amount and type \n" +
                "       -if you are correct, you win twice your bet, \n" +
                "       -otherwise you lose your bet\n" +
                "A bet of 0 ends the game");
        this.potAmount = 100;
        this.betAmount = 0;
        this.betType = "";
        this.input = new Scanner(System.in);
    }

    // prompts the user to enter a valid bet amount and type
    public void getBetFromUser() {
        System.out.print("\nYour current pot is " + this.potAmount + "\nEnter your bet amount: ");
        int i = input.nextInt();

        while (i > this.potAmount || i < 0) {
            System.out.print("Error - cannot bet less than 0 or more than " + this.potAmount + "...Enter your bet amount:  ");
            i = input.nextInt();
        }
        this.betAmount=i;
        if (this.betAmount != 0) {
            System.out.print("Enter your bet type: ");
            this.betType = input.next().toLowerCase();
        }
        while (!this.betType.equals("odd") && !this.betType.equals("even") && !this.betType.equals("high") && !this.betType.equals("low") && !(this.betAmount == 0)) {
            System.out.print("Please enter odd, even, high, or low ....Enter your bet type: ");
            this.betType = input.next().toLowerCase();
        }

    }

    // logic for the game
    public void playGame() {
        Die[] d = new Die[3];

        String win = "You WIN....double your bet\n";
        String lose = "You LOSE....your bet\n";

        while (this.potAmount != 0) {
            getBetFromUser();

            if (this.betAmount==0){
                System.out.println("You end the game with pot of " + this.potAmount);
                return;
            }

            for (int i = 0; i < 3; i++) {
                d[i] = new Die();
                d[i].rollDie();
            }

            System.out.print("Your dies are: ");
            d[0].displayDie();
            System.out.print(" and ");
            d[1].displayDie();
            System.out.print(" and ");
            d[2].displayDie();
            System.out.print("\n");


            int sum = 0;
            for (Die i : d) {
                sum += i.getValue();
            }

            switch (this.betType) {
                case "odd":
                    if (sum % 2 != 0) {
                        System.out.print(win);
                        this.potAmount += this.betAmount;
                    } else {
                        System.out.print(lose);
                        this.potAmount -= this.betAmount;
                    }
                    break;
                case "even":
                    if (sum % 2 == 0) {
                        System.out.print(win);
                        this.potAmount += this.betAmount;
                    } else {
                        System.out.print(lose);
                        this.potAmount -= this.betAmount;
                    }
                    break;
                case "high":
                    if (sum >= 9) {
                        System.out.print(win);
                        this.potAmount += this.betAmount;
                    } else {
                        System.out.print(lose);
                        this.potAmount -= this.betAmount;
                    }
                    break;
                case "low":
                    if (sum < 9) {
                        System.out.print(win);
                        this.potAmount += this.betAmount;
                    } else {
                        System.out.print(lose);
                        this.potAmount -= this.betAmount;
                    }
                    break;
            }
        }
        System.out.print("\nYour current pot is " + this.potAmount + "\nYou end the game with pot of " + (this.potAmount+this.betAmount));
    }
}
