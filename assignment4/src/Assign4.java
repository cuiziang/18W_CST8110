public class Assign4 {

    public static void main(String[] args) {
		/*----------------------------
		 * Test 1: default constructor
		----------------------------*/
        StringArray test1 = new StringArray();
        boolean test1Successful = true;
        if (!test1.isEmpty()) {
            System.out.println("Test 1a: default object is not empty");
            test1Successful = false;
        }
        if (test1.size() != 0) {
            System.out.println("Test 1b: default object size is not zero");
            test1Successful = false;
        }
        if (test1.capacity() != 10) {
            System.out.println("Test 1c: default object capacity is not ten");
            test1Successful = false;
        }
        if (test1Successful) {
            System.out.println("Test 1: Pass");
        }

		/*----------------------------
		 * Test 2: initial constructor
		----------------------------*/
        StringArray test2 = new StringArray(5);
        boolean test2Successful = true;
        if (!test2.isEmpty()) {
            System.out.println("Test 2a: initial object is not empty");
            test2Successful = false;
        }
        if (test2.size() != 0) {
            System.out.println("Test 2b: initial object size is not five");
            test2Successful = false;
        }
        if (test2.capacity() != 5) {
            System.out.println("Test 2c: initial object capacity is not ten");
            test2Successful = false;
        }
        if (test2Successful) {
            System.out.println("Test 2: Pass");
        }
		/*----------------------------
		 * Test 3: copy constructor
		----------------------------*/
        StringArray test3 = new StringArray(test2);
        boolean test3Successful = true;
        if (!test3.isEmpty()) {
            System.out.println("Test 3a: copy object is not empty");
            test3Successful = false;
        }
        if (test3.size() != 0) {
            System.out.println("Test 3b: copy object size is not zero");
            test3Successful = false;
        }
        if (test3.capacity() != 5) {
            System.out.println("Test 3c: copy object capacity is not five");
            test3Successful = false;
        }
        if (test3Successful) {
            System.out.println("Test 3: Pass");
        }
		/*----------------------------
		 * Test 4: add(String s)
		----------------------------*/
        StringArray test4 = new StringArray();
        boolean test4Successful = true;
        test4.add("Test 4");
        if (!(test4.get(test4.size() - 1).equals("Test 4"))) {
            System.out.println("Test 4a: added object's end is not equal to Test 4");
            test4Successful = false;
        }
        if (test4Successful) {
            System.out.println("Test 4: Pass");
        }
		/*----------------------------
		 * Test 5: add(int index, String s)
		----------------------------*/
        StringArray test5 = new StringArray();
        boolean test5Successful = true;
        test5.add("Test 5.1");
        test5.add("Test 5.2");
        test5.add(1, "Test 5.3");
        if (!((test5.get(0).equals("Test 5.1")))) {
            System.out.println("Test 5a: the index 0 is not equal to Test 5.1");
            test4Successful = false;
        }
        if (!((test5.get(1).equals("Test 5.3")))) {
            System.out.println("Test 5b: the index 1 is not equal to Test 5.3");
            test4Successful = false;
        }
        if (!((test5.get(2).equals("Test 5.2")))) {
            System.out.println("Test 5c: the index 2 is not equal to Test 5.2");
            test4Successful = false;
        }
        if (test4Successful) {
            System.out.println("Test 5: Pass");
        }
		/*----------------------------
		 * Test 6: clear()
		----------------------------*/
        StringArray test6 = new StringArray();
        boolean test6Successful = true;
        test5.add("Test 6.1");
        test5.add("Test 6.2");
        test5.add(1, "Test 6.3");
        test6.clear();
        if (!(test6.size() == 0)) {
            System.out.println("Test 6a: the size of StringArray is not equal to 0");
            test6Successful = false;
        }
        if (test6Successful) {
            System.out.println("Test 6: Pass");
        }
		/*----------------------------
		 * Test 7: contains(String s) and indexOf(String s)
		----------------------------*/
        StringArray test7 = new StringArray();
        boolean test7Successful = true;
        test7.add("Test 7.1");
        test7.add("Test 7.2");
        test7.add(1, "Test 7.3");
        if (!(test7.contains("Test 7.3"))) {
            System.out.println("Test 7a: StringArray do not include 7.3");
            test7Successful = false;
        }
        if (test7Successful) {
            System.out.println("Test 7: Pass");
        }
		/*----------------------------
		 * Test 8: remove(int index) and remove(String s)
		----------------------------*/
        StringArray test8 = new StringArray();
        boolean test8Successful = true;
        test8.add("Test 8.1");
        test8.add("Test 8.2");
        test8.add(1, "Test 8.3");
        test8.remove(1);
        if (!((test8.get(1).equals("Test 8.2")))) {
            System.out.println("Test 8a: the index 1 is not equal to Test 8.2");
            test8Successful = false;
        }
        test8.remove("Test 8.1");
        if (!((test8.get(0).equals("Test 8.2")))) {
            System.out.println("Test 8b: the index 1 is not equal to Test 8.2");
            test8Successful = false;
        }
        if (test8Successful) {
            System.out.println("Test 8: Pass");
        }
		/*----------------------------
		 * Test 9: get(int index) and set(int index, String s)
		----------------------------*/
        StringArray test9 = new StringArray();
        boolean test9Successful = true;
        test9.add("Test 9.1");
        test9.add("Test 9.2");
        test9.add(1, "Test 9.3");
        test9.remove(1);
        if (!((test9.get(1).equals("Test 9.2")))) {
            System.out.println("Test 9a: the index 1 is not equal to Test 9.2");
            test8Successful = false;
        }
        if (test9Successful) {
            System.out.println("Test 9: Pass");
        }
		/*----------------------------
		 * Test 10: ensureCapacity(int minCapacity)
		----------------------------*/
        StringArray test10 = new StringArray();
        boolean test10Successful = true;
        test10.ensureCapacity(20);
        if (!(test10.capacity() == 20)) {
            System.out.println("Test 10a: it is out of capacity");
            test10Successful = false;
        }
        test10.ensureCapacity(30);
        if (!(test10.capacity() == 30)) {
            System.out.println("Test 10b: it is in capacity");
            test10Successful = false;
        }
        if (test10Successful) {
            System.out.println("Test 10: Pass");
        }
        /*----------------------------
		 * Test 11: indexOf(String s)
		----------------------------*/
        StringArray test11 = new StringArray();
        boolean test11Successful = true;
        test11.add("Test 11.1");
        test11.add("Test 11.2");
        test11.add(1, "Test 11.3");
        if (!(test11.indexOf("Test 11.3") == 1)) {
            System.out.println("Test 11a: the index 1 is not equal to Test 11.3");
            test11Successful = false;
        }

        if (test11Successful) {
            System.out.println("Test 11: Pass");
        }
    }
}
