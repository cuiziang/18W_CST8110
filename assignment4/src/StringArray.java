
public class StringArray {

    // The current capacity of the array
    private int capacity;
    // The current size of the array
    private int size;
    // The current String array
    public String[] stringArray;

    // Default constructor, Constructs an empty StringArray with an initial capacity of ten.
    public StringArray() {
        this(10);
    }

    // Initial constructor, Constructs an empty StringArray with the specified initial capacity.
    public StringArray(int initialCapacity) {
        this.capacity = initialCapacity;
        this.size = 0;
        stringArray = new String[capacity];
    }

    // Copy constructor, Constructs a StringArray that is a copy of a given StringArray
    public StringArray(StringArray sa) {
        this.capacity = sa.capacity;
        this.size = sa.size;
        this.stringArray = new String[capacity];
        for (int i = 0; i < sa.stringArray.length; i++) {
            this.stringArray[i] = sa.stringArray[i];
        }

    }

    // Appends the specified string to the end of this StringArray. Increases capacity if needed.
    public void add(String s) {
        this.add(this.size, s);
    }

    // Inserts the specified string at the specified position in this StringArray. Increases capacity if needed.
    public void add(int index, String s) {
        if (index == this.size || index == 0) {
            this.size++;
            this.ensureCapacity(this.size);
            String[] stringArrayOld = this.stringArray;
            this.stringArray = new String[size];
            for (int i = 0; i < index; i++) {
                this.stringArray[i] = stringArrayOld[i];
            }
            this.stringArray[index] = s;
        } else if (index < this.size) {
            this.size++;
            this.ensureCapacity(this.size);
            String[] stringArrayOld = this.stringArray;
            this.stringArray = new String[size];
            for (int i = 0; i < index; i++) {
                this.stringArray[i] = stringArrayOld[i];
            }
            this.stringArray[index] = s;
            for (int i = index; i < size - 1; i++) {
                this.stringArray[i + 1] = stringArrayOld[i];
            }
        } else {
            System.out.println("Error");
        }
    }

    // Returns the capacity of this StringArray.
    public int capacity() {
        return this.capacity;
    }

    // Removes all of the Strings from this StringArray.
    public void clear() {
        for (int i = 0;i<this.size;i++) {
            this.remove(0);
        }
    }

    // Returns true if this StringArray contains the specified String.
    public boolean contains(String s) {
        for (int i = 0; i < this.stringArray.length; i++) {
            if (this.stringArray[i].equals(s)) {
                return true;
            }
        }
        return false;
    }

    // To ensure that it can hold at least the number of Strings specified by the minimum capacity argument.
    public void ensureCapacity(int minCapacity) {
        if (minCapacity > this.capacity) {
            this.capacity = minCapacity;
        }
    }

    // Returns the String at the specified position in this StringArray.
    public String get(int index) {

        return this.stringArray[index];
    }

    // Returns the index of the first occurrence of the specified String in this StringArray, or -1 if this StringArray does not contain the String.
    public int indexOf(String s) {
        for (int i = 0; i < this.stringArray.length; i++) {
            if (this.stringArray[i].equals(s)) {
                return i;
            }
        }
        return -1;
    }

    // Returns true if this StringArray contains no Strings.
    public boolean isEmpty() {
        return size == 0;
    }

    // Removes the String at the specified position in this StringArray. Return the String previously at the specified position
    public String remove(int index) {
        String s = this.stringArray[index];
        String[] stringArrayOld = this.stringArray;
        this.stringArray = new String[this.size-1];
        for (int i = 0; i < index; i++) {
            this.stringArray[i] = stringArrayOld[i];
        }
        for (int i = index; i < size-1; i++) {
            this.stringArray[i] = stringArrayOld[i + 1];
        }
        this.size--;
        return s;
    }

    // Removes the first occurrence of the specified String from this StringArray, if it is present. Return true if this StringArray contained the specified String.
    public void remove(String s) {
        int index = this.indexOf(s);
        this.remove(index);
    }

    // Replaces the String at the specified position in this StringArray with the specified String. Return the String previously at the specified position. Increases capacity if needed.
    public String set(int index, String s) {
        this.ensureCapacity(1);
        String t = this.stringArray[index];
        String[] stringArrayT = this.stringArray;
        if (index > this.size - 1) {
            this.size = index + 1;
        }
        this.stringArray = new String[capacity];
        for (int i = 0; i < size - 1; i++) {
            this.stringArray[i] = stringArrayT[i];
        }
        return t;
    }

    // Returns the number of Strings in this StringArray.
    public int size() {
        return this.size;
    }
}
