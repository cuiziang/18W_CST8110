import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        Employee e1 = new Employee("Erich", "Gamma", 123456.78);

        Employee e2 = new Employee();
        System.out.println("First Name?");
        e2.setFirstName(input.nextLine());
        System.out.println("Last Name?");
        e2.setLastName(input.nextLine());
        System.out.println("Monthly Salary?");
        e2.setMonthlySalary(input.nextDouble());

        e1.display();
        System.out.println(e2.getFirstName()+" "+e2.getLastName()+": "+e2.getMonthlySalary());

    }
}
