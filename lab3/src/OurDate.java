import java.util.*;
import java.util.Calendar;

/**  This program prompts user to enter a numbers, then displays the next number
 *   Author:   Howard Rosenblum
 *   CST8110 Introduction to Computer Programming
 */


public class OurDate{
    private int year = 2000;
    private int month = 1;
    private int day = 1;


    public void inputYear() {
        Scanner keyboard = new Scanner (System.in);
        System.out.println("Enter a year: ");
        year = keyboard.nextInt();
        }

        public void inputMonth() {
        Scanner keyboard = new Scanner (System.in);
        System.out.println("Enter a month: ");
        month = keyboard.nextInt();
        }

        public void inputDay( ) {
        Scanner keyboard = new Scanner (System.in);
        System.out.println("Enter a day: ");
        day = keyboard.nextInt();
        }


        public void displayDate() {
        System.out.println (year+"/"+month+"/"+day);
        }

        public int calcDays() {
        return ((year-2000)*360+(month-1)*30+(day-1));
        }
}


