/*
* This program prompts user to enter the score of every assessment, then calculate the final score.
final grade
* Author:   Ziang Cui
* CST8110 Introduction to Computer Programming
* */
public class Lab4 {
    public static void main(String[] args) {

        MakeCalculator mc = new MakeCalculator();
        mc.getValues();
        mc.calculateGrades();
        mc.displayGrades();
    }
}