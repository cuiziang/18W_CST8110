import java.text.DecimalFormat;
import java.util.Scanner;

public class MakeCalculator {
    private double test;
    private double hybird;
    private double lab;
    private double finalExam;
    private double theory;
    private double practical;
    private double finalGrade;
    private Scanner input;
    private DecimalFormat formatter = new DecimalFormat("#.##%");

    public MakeCalculator(){
        System.out.println  ("Welcome to the CST8101 Final Mark Calculator\n");
        this.input = new Scanner(System.in);
    }

    public void getValues(){
        System.out.println  ("Enter your Lab Assignments mark out of 25: ");
        this.lab = input.nextDouble();
        System.out.println  ("Enter your Hybrid Activities / Quizzes mark out of 20: ");
        this.hybird = input.nextDouble();
        System.out.println  ("Enter your Test mark out of 25: ");
        this.test = input.nextDouble();
        System.out.println  ("Enter your Final Assessment (Exam) mark out of 30: ");
        this.finalExam = input.nextDouble();
    }

    public void calculateGrades(){
        this.theory=(this.test+this.finalExam)/55;
        this.practical=(this.lab+this.hybird)/45;
        this.finalGrade=(this.test+this.finalExam+this.lab+this.hybird)/100;
    }

    public void displayGrades(){
        System.out.println("\nTheory grade: "+this.formatter.format(this.theory));
        System.out.println("Practical grade: "+this.formatter.format(this.practical));
        System.out.println("Final grade: "+this.formatter.format(this.finalGrade));
    }
}
