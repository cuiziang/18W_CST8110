import java.util.Scanner;
/*  This program prompts user to enter a numbers, then displays the next number
 Author:   Ziang Cui
 CST8110 Introduction to Computer Programming
 */
public class Calculator {

        public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        double fst ;
        String o = "";
        double scd;

        System.out.print("Simple calculator\n");
        System.out.print("First number:");
        fst = s.nextDouble();

        System.out.print("Please enter an operation:");
        o = s.next();
        while (!(o.equals("+") || o.equals("-") || o.equals("*") || o.equals("/") || o.equals("%"))) {
            System.out.print("Please enter a valid operation:");
            o = s.next();
        }

        System.out.print("Second number:");
        scd = s.nextDouble();
        while ((o.equals("/") && scd==0) || (o.equals("%") && scd==0)) {
            System.out.print("Please pick a non-zero number:");
            scd = s.nextDouble();
        }

        switch (o){
            case "+":
                System.out.println(fst + " + " + scd + " = " + (fst+scd));
                break;
            case "-":
                System.out.println(fst + " - " + scd + " = " + (fst-scd));
                break;
            case "*":
                System.out.println(fst + " * " + scd + " = " + (fst*scd));
                break;
            case "/":
                System.out.println(fst + " / " + scd + " = " + (fst/scd));
                break;
            case "%":
                System.out.println(fst + " % " + scd + " = " + (fst%scd));
                break;
        }

    }
}
