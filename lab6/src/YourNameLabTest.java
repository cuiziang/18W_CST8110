import java.util.Scanner;

public class YourNameLabTest {
    private int value1;
    private int value2;
    private int gcf;
    private Scanner s;

    public YourNameLabTest(){
        value1 = 0;
        value1 = 0;
        gcf = 0;
        s = new Scanner(System.in);
    }

    public void getValues(){
        System.out.print("Enter value1:");
        this.value1 = s.nextInt();
        while (this.value1<=0) {
            System.out.print("Enter value1 that is greater than 0:");
            this.value1=s.nextInt();
        }

        System.out.print("Enter value2:");
        this.value2 = s.nextInt();
        while (this.value2<=0) {
            System.out.print("Enter value2 that is greater than 0:");
            this.value2=s.nextInt();
        }

    }

    public void calculateGcf(){
        int a = this.value1;
        int b = this.value2;
        while (b!=0){
            int temp = a % b;
            a=b;
            b=temp;
        }
        this.gcf = a;
    }

    public void display(){
        System.out.println("The greatest common factor of " + this.value1 + " and " + this.value2 + " is " + this.gcf);
    }

}
