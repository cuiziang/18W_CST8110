import java.util.Scanner;

public class FibonacciNumbers {

    private int numberofNumbers;
    private int a;
    private int b;
    private int[] fbNumbers;

    public FibonacciNumbers() {
        this.numberofNumbers = 0;
        this.a = 1;
        this.b = 1;
    }

    public void getNumberFromUser() {
        Scanner s = new Scanner(System.in);
        System.out.println("How many Fibonacci numbers do you want? ");
        this.numberofNumbers = s.nextInt();
        while (this.numberofNumbers < 2 ) {
            System.out.println("You need to generate at least two");
            this.numberofNumbers = s.nextInt();
        }
        this.fbNumbers = new int[this.numberofNumbers];
    }

    private int nextNumber() {
        int temp = this.b;
        this.b = this.b + this.a;
        a = temp;
        return this.b;
    }

    public void createFibonacciNumbers() {
        this.fbNumbers[0]=this.a;
        this.fbNumbers[1]=this.b;
        for (int i = 2;i<this.numberofNumbers;i++){
            this.fbNumbers[i] = this.nextNumber();
        }
    }

    public void printFibonacciNumbers() {
        System.out.println("The first "+ this.numberofNumbers +" Fibonacci numbers in reverse order are: ");
        for (int i=numberofNumbers-1;i>=0;i--){
            System.out.println(this.fbNumbers[i]);
        }
    }

}