public class Lab7 {

    public static void main(String[] args) {
        System.out.println("Program developed by Ziang Cui");
        FibonacciNumbers f = new FibonacciNumbers();
        f.getNumberFromUser();
        f.createFibonacciNumbers();
        f.printFibonacciNumbers();
    }
}
