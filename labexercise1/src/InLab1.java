import java.util.Scanner;

/**  This program will calculate how many US dollars you can get from some Canadian dollars
 *   Author:   Ziang Cui
 *   CST8110 Introduction to Computer Programming
 */

public class InLab1 {
    public static void main(String[] args) {
        float canadianDollar, rate;

        Scanner input = new Scanner(System.in);


        System.out.print("This program, written by Ziang Cui, Will calculate how many US dollars you can get from some Canadian dollars\n");
        System.out.print("Enter the number of Canadian dollars you have: ");
        canadianDollar = input.nextFloat();
        System.out.print("Enter the current exchange rate: ");
        rate = input.nextFloat();
        System.out.print("$" + canadianDollar + " CAN equals $" + canadianDollar*rate+ " US");
    }
}
