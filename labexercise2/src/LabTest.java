import java.util.Scanner;

public class LabTest {
    private int value1;
    private int value2;
    private Scanner s;

    public LabTest() {
        this.value1 = 0;
        this.value2 = 0;
        s = new Scanner(System.in);
    }

    public void getValues() {
        System.out.print("Enter the first value: ");
        this.value1 = s.nextInt();
        while (this.value1<1) {
            System.out.print("Invalid entry...Enter the first value: ");
            this.value1 = s.nextInt();
        }

        System.out.print("Enter the second value: ");
        this.value2 = s.nextInt();
        while (this.value2<1){
            System.out.print("Invalid entry...Enter the second value: ");
            this.value2 = s.nextInt();
        }
    }

    public void display(){
        int large = this.value1;
        int small = this.value2;
        if (large<small){
            int t = large;
            large = small;
            small = t;
        }
        System.out.println(large + " - " + small + " = " + (large-small));
    }
}
